## 下载基因组数据
```Bash
$ mkdir data
$ cd data/
$ wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
$ md5sum GCF_000005845.2_ASM584v2_genomic.fna.gz # 验证文件是否下载完整c13d459b5caa702ff7e1f26fe44b8ad7
```

## 生成测序数据

真实的测序数据可以从ncbi下载，但是真实测序数据并不知道答案，所以该项目选择使用读长模拟工具（mason2）模拟读长。
- 安装mason2
```Bash
# 没有conda的话需要安装。
$ conda create -n bio
$ conda activate bio
$ conda install -c bioconda mason
```
- 使用mason2模拟二代读长，模拟1M条100长的illumina读长，读长文件为reads.fa, 答案为alignments.sam。

```Bash
$ gunzip GCF_000005845.2_ASM584v2_genomic.fna.gz
$ mason_simulator -ir GCF_000005845.2_ASM584v2_genomic.fna -n 1000000 --illumina-read-length 100 -o reads.fa -oa alignments.sam
```

## 写程序
- 创建目录
```Bash
$ mkdir release debug log src
```
- git初始化 + 添加[生信相关C语言库-klib库](https://github.com/attractivechaos/klib)
```Bash
$ git init
$ git submodule add https://github.com/attractivechaos/klib.git
```
- 测试klib库是否正常使用。
- 开始写项目，程序命名为`XXaligner`, 且程序支持以下命令。
    - 程序构建索引， `XXaligner idx -k 19 ref.fa >ref.fa.idx`
    - 程序比对，`XXaligner aln ref.fa reads.fq > alignment.sam`
