- Linux编程实验报告7
- 实验内容：VIM与GCC
- 学号：
- 姓名：
- 日期：2022.10.13

### 1. 编辑
使用VIM创建text.txt文件，并用VIM对text.txt文件进行编辑，编辑内容如下
```text
"...We're waiting for content before the site can go live...

...If you are content with this, let's go ahead with it...

...We'll launch as soon as we have the content...”
```
> 操作如下：
> 1. 步骤1
> 2. 步骤2
> 3. 步骤2

### 2. 查找与替换
替换文本中第1个和第3个“content”为“copy”
将所有“We”替换为“You”
```text
"...We're waiting for content before the site can go live...

...If you are content with this, let's go ahead with it...

...We'll launch as soon as we have the content...”
```

> 操作如下：
> 1. 步骤1
> 2. 步骤2
> 3. 步骤2

### 3. 行首和行尾插入字符
在文件行首和行尾分别加入“;”
- 修改前:

```javascript
var foo = 1
var bar = 'a'
var foobar = foo + bar
```

- 修改后:

```javascript
;var foo = 1;
;var bar = 'a';
;var foobar = foo + bar;
```

> 操作如下：
> 1. 步骤1
> 2. 步骤2
> 3. 步骤2

### 4. 复制与删除
复制文件中`<tr></tr>`间的代码：
- 复制前
```HTML
<table>
  <tr>
    <td>keystrokes</td>
    <td>buffer contents</td>
  </tr>
</table>
```
- 复制后
```HTML
<table>
  <tr>
    <td>keystrokes</td>
    <td>buffer contents</td>
  </tr>
  <tr>
    <td>keystrokes</td>
    <td>buffer contents</td>
  </tr>
</table>
```
> 操作如下：
> 1. 步骤1
> 2. 步骤2
> 3. 步骤2

如果想要将复制后的代码还原为复制前的代码，应如何操作？

> 操作如下：
> 1. 步骤1
> 2. 步骤2
> 3. 步骤2

### 5. 使用GCC编译C程序
- 编写number.c程序，用户输入整数n，程序返回n的绝对值和平方跟。
- 使用gcc编译并执行该程序。
- 使用gcc分步编译number.c，并生成number.i、number.s、number.o，并比较文件差异。

> 操作如下：
> 1. 步骤1
> 2. 步骤2
> 3. 步骤2